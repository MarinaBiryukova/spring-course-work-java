package com.example.demo.service_2;

import com.example.demo.service_3.Schedule;
import com.example.demo.service_3.Service_3;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.example.demo.service_1.Options;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

@RestController
public class Service_2 {
    private int COUNT = 300;

    @GetMapping("/jsonStartSchedule")
    public String getJsonSchedule(@RequestParam(value = "count") int count) {
        String url = "http://localhost:8080/startSchedule?count=" + count;
        URL obj = null;
        try {
            obj = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        BufferedReader in = null;
        try {
            HttpURLConnection connection = null;
            if (obj != null) {
                connection = (HttpURLConnection) obj.openConnection();
            }
            if (connection != null) {
                connection.setRequestMethod("GET");
                in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        String path = ".\\src\\main\\resources\\schedule.json";
        FileWriter jsonFile = null;
        String temp = "";
        try {
            jsonFile = new FileWriter(path);
            if (in != null) {
                temp = in.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONArray array = null;
        try {
            array = new JSONArray(temp);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ObjectMapper mapper = new ObjectMapper();
        if (array != null) {
            for (int i = 0; i < array.length(); i++) {
                try {
                    String s = array.getString(i);
                    if (jsonFile != null) {
                        jsonFile.write(s + "\n");
                    }
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Do you want to add ship? Enter 0 for skip, 1 for adding:");
        Scanner scanner = new Scanner(System.in);
        int chose = scanner.nextInt();
        while (chose != 0) {
            if (chose != 1) {
                System.out.println("Invalid command input");
                System.out.println("Do you want to add ship? Enter 0 for skip, 1 for adding:");
                chose = scanner.nextInt();
                continue;
            }
            try {
                temp = mapper.writeValueAsString(getShip());
                if (jsonFile != null) {
                    jsonFile.write(temp + "\n");
                }
                COUNT++;
            } catch (IOException | RuntimeException e) {
                e.printStackTrace();
            }
            System.out.println("Do you want to add ship? Enter 0 for skip, 1 for adding:");
            chose = scanner.nextInt();
        }
        try {
            if (jsonFile != null) {
                jsonFile.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return path;
    }

    @GetMapping("/scheduleByDoc")
    public ArrayList<Options.Ship> getScheduleByDoc(@RequestParam("name") String docName) throws FileNotFoundException {
        ArrayList<Options.Ship> schedule = new ArrayList<>();
        FileReader jsonFile = new FileReader(docName);
        BufferedReader reader = new BufferedReader(jsonFile);
        ObjectMapper mapper = new ObjectMapper();
        String obj = null;
        try {
            obj = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (obj != null) {
            Options.Ship ship;
            try {
                mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                ship = mapper.readValue(obj, Options.Ship.class);
                schedule.add(ship);
                obj = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return schedule;
    }

    @PostMapping("/finalStatistic")
    public void finalScheduleToJson() {
        Schedule schedule = Service_3.getStatistic(COUNT);
        String path = ".\\src\\main\\resources\\finalSchedule.json";
        FileWriter jsonFile = null;
        try {
            jsonFile = new FileWriter(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ObjectMapper mapper = new ObjectMapper();
        String result;
        try {
            result = mapper.writeValueAsString(schedule);
            if (jsonFile != null) {
                jsonFile.write(result);
                jsonFile.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static Options.Ship getShip() throws RuntimeException {
        Options.Ship ship = new Options.Ship();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter time of arrive (dd:hh:mm): ");
        String time = scanner.nextLine();
        ship.setTime(getTime(time));
        System.out.println("Enter name of ship:");
        String name = scanner.nextLine();
        ship.setName(name);
        System.out.println("Enter type of cargo: ");
        String type = scanner.nextLine();
        if (!type.equals("loose") && !type.equals("liquid") && !type.equals("container")) {
            throw new RuntimeException("Invalid type of cargo");
        }
        ship.setType(type);
        System.out.println("Enter count of cargo:");
        int countOfCargo = scanner.nextInt();
        ship.setCountOfCargo(countOfCargo);
        ship.recalculateTimeOfUnloading(1);
        return ship;
    }

    static Options.Time getTime(String line) throws RuntimeException {
        Options.Time time = new Options.Time(0,0,0);
        String[] times = line.split(":");
        if (times.length != 3) {
            throw new RuntimeException("Invalid time of arrive");
        }
        int days, hours, minutes;
        days = Integer.parseInt(times[0]);
        if (days < 1 || days > 30) {
            throw new RuntimeException("Invalid time of arrive");
        }
        hours = Integer.parseInt(times[1]);
        if (hours < 0 || hours > 23) {
            throw new RuntimeException("Invalid time of arrive");
        }
        minutes = Integer.parseInt(times[2]);
        if (minutes < 0 || minutes > 59) {
            throw new RuntimeException("Invalid time of arrive");
        }
        time.setDays(days);
        time.setHours(hours);
        time.setMinutes(minutes);
        return time;
    }
}
