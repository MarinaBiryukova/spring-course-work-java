package com.example.demo.service_1;

import java.io.PrintStream;

public class Options {
    static String[] types = {"loose", "liquid", "container"};
    static Integer[] performances = {157, 174, 115};  // tons/containers per hour
    static String[] names = {"Atlantic Causeway", "Biscaglia", "Colombo Express", "CMA CGM Medea", "Histria Azure",
            "MV Avenue Star", "MT Haven", "Seabulk Pride", "U-Sea Colonsay", "Anglo-Colombian"};

    public static class Time implements Comparable<Time> {
        private int hours, minutes, days;

        public Time() {
            this.minutes = (int) (Math.random() * 51) / 10 * 10;
            this.hours = (int) (Math.random() * 24);
            this.days = (int) (Math.random() * 30) + 1;
        }

        public Time(Integer days, Integer hours, Integer minutes) {
            this.minutes = minutes;
            this.hours = hours;
            this.days = days;
        }

        public void addMinutes(Integer minutes) {
            int currentMinutes = this.days * 1440 + this.hours * 60 + this.minutes + minutes;
            if (currentMinutes >= 1440) {
                this.days = currentMinutes / 1440;
                currentMinutes %= 1440;
            } else {
                this.days = 0;
            }
            if (currentMinutes >= 60) {
                this.hours = currentMinutes / 60;
                currentMinutes %= 60;
            } else {
                this.hours = 0;
            }
            this.minutes = currentMinutes;
        }

        public void addTime(Time time) {
            this.addMinutes(time.days * 1440 + time.hours * 60 + time.minutes);
        }

        public Integer getHours() {
            return this.hours;
        }

        public Integer getMinutes() {
            return this.minutes;
        }

        public Integer getDays() {
            return this.days;
        }

        public void setHours(int hours) {
            this.hours = hours;
        }

        public void setMinutes(int minutes) {
            this.minutes = minutes;
        }

        public void setDays(int days) {
            this.days = days;
        }

        public Integer fullMinutes() {
            return (days * 1440 + hours * 60 + minutes);
        }

        public void print(PrintStream output) {
            output.printf("%02d:%02d:%02d", days, hours, minutes);
        }

        public Time divide(Integer number) {
            int days = 0;
            int hours = 0;
            Integer sum = this.days * 1440 + this.hours * 60 + this.minutes;
            sum /= number;
            if (sum >= 1440) {
                days = sum / 1440;
                sum %= 1440;
            }
            if (sum >= 60) {
                hours = sum / 60;
                sum %= 60;
            }
            int minutes = sum;
            return (new Time(days, hours, minutes));
        }

        public Time subtraction(Time time) {
            if (this.days - time.days < 0) {
                this.print(System.out);
                System.out.print("---");
                time.print(System.out);
                System.out.println();
                throw new ArithmeticException("Illegal subtraction");
            }
            int temp = this.fullMinutes() - time.fullMinutes();
            Time result = new Time(0, 0, 0);
            if (temp >= 1440) {
                result.days = temp / 1440;
                temp %= 1440;
            }
            if (temp >= 60) {
                result.hours = temp / 60;
                temp %= 60;
            }
            result.minutes = temp;
            return result;
        }

        @Override
        public int compareTo(Time time) {
            return this.fullMinutes() - time.fullMinutes();
        }
    }

    public static class Ship implements Comparable<Ship> {
        private Time time;
        private String name;
        private String type;
        private int countOfCargo;
        private Time timeOfUnloading;

        public Ship() {
            this.name = names[(int) (Math.random() * 10)];
            this.type = types[(int) (Math.random() * 3)];
            this.countOfCargo = (int) (Math.random() * 2001) + 300;
            this.timeOfUnloading = getTimeOfUnloading(this.countOfCargo, this.type, 1);
            this.time = new Time();
        }

        public Ship(Time time, String name, String type, Integer countOfCargo, Time timeOfUnloading) {
            this.time = time;
            this.name = name;
            this.type = type;
            this.countOfCargo = countOfCargo;
            this.timeOfUnloading = timeOfUnloading;
        }

        public Ship(Ship ship) {
            this(ship.time, ship.name, ship.type, ship.countOfCargo, ship.timeOfUnloading);
        }

        private Time getTimeOfUnloading(double countOfCargo, String type, Integer countOfCranes) {
            double performance = 0.0;
            for (int i = 0; i < types.length; i++) {
                if (types[i].equals(type)) {
                    performance = performances[i];
                }
            }
            performance *= countOfCranes;
            int minutes = (int) ((countOfCargo / performance) % 1 * 60);
            int hours = 0;
            int days = 0;
            if (minutes % 10 > 0) {
                if ((minutes / 10 + 1) * 10 == 60) {
                    hours += 1;
                    minutes = 0;
                } else {
                    minutes = (minutes / 10 + 1) * 10;
                }
            }
            if ((int) (countOfCargo / performance) >= 24) {
                days = (int) (countOfCargo / performance) / 24;
                hours = (int) (countOfCargo / performance) % 24;
            } else {
                hours = (int) (countOfCargo / performance);
            }
            return new Time(days, hours, minutes);
        }

        public void print(PrintStream output) {
            output.print("Name is " + this.name + ". Arrive time: " + this.time.days + " day " + this.time.hours
                    + "." + this.time.minutes + " Cargo type is " + this.type + ". Count of cargo " + this.countOfCargo
                    + ". Time of standing " + this.timeOfUnloading.days + "." + this.timeOfUnloading.hours + "."
                    + this.timeOfUnloading.minutes + "\n");
        }

        public Time getTime() {
            return time;
        }

        public String getName() {
            return name;
        }

        public String getType() {
            return type;
        }

        public Integer getCountOfCargo() {
            return countOfCargo;
        }

        public Time getTimeOfUnloading() {
            return timeOfUnloading;
        }

        @Override
        public int compareTo(Ship ship) {
            return (this.time.compareTo(ship.time) + this.name.compareTo(ship.name) + this.type.compareTo(ship.type)
                    + (this.countOfCargo - ship.countOfCargo));
        }

        public void addDaysToArrive(Integer days) {
            this.time.days += days;
            if (this.time.days < 0) {
                this.time.days = 0;
            }
        }

        public void addMinutesToUnloading(Integer minutes) {
            this.timeOfUnloading.addMinutes(minutes);
        }

        public boolean isLinger(Ship ship, Integer waiting1, Integer waiting2) {
            waiting1 += this.timeOfUnloading.days * 1440 + this.timeOfUnloading.hours * 60
                    + this.timeOfUnloading.minutes;
            Time temp = new Time(this.time.days, this.time.hours, this.time.minutes);
            temp.addMinutes(waiting1);
            Time arrive = new Time(ship.time.days, ship.time.hours, ship.time.minutes);
            arrive.addMinutes(waiting2);
            return ((arrive.days < temp.days) || (arrive.days == temp.days && arrive.hours < temp.hours)
                    || (arrive.days == temp.days && arrive.hours == temp.hours && arrive.minutes < temp.minutes));
        }

        public Time getTimeOfDelay(Ship ship, Integer waiting1, Integer waiting2) {
            Time result = new Time(0, 0, 0);
            Time temp = new Time(this.time.days, this.time.hours, this.time.minutes);
            temp.addMinutes(waiting1);
            Time arrive = new Time(ship.time.days, ship.time.hours, ship.time.minutes);
            arrive.addMinutes(waiting2);
            if ((arrive.days < temp.days) || (arrive.days == temp.days && arrive.hours < temp.hours)
                    || (arrive.days == temp.days && arrive.hours == temp.hours && arrive.minutes < temp.minutes)) {
                result.addTime(this.timeOfUnloading);
                return result;
            } else {
                temp.addTime(this.timeOfUnloading);
            }
            result = temp.subtraction(arrive);
            return result;
        }

        public void recalculateTimeOfUnloading(Integer countOfCranes) {
            if (countOfCranes == 2) {
                timeOfUnloading = timeOfUnloading.divide(countOfCranes);
                if (timeOfUnloading.minutes % 10 != 0) {
                    timeOfUnloading.addMinutes(10 - (timeOfUnloading.minutes % 10));
                }
            }
            else {
                timeOfUnloading = getTimeOfUnloading(this.countOfCargo, this.type, countOfCranes);
            }
        }

        public void setTime(Time time) {
            this.time = time;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setType(String type) {
            this.type = type;
        }

        public void setCountOfCargo(Integer countOfCargo) {
            this.countOfCargo = countOfCargo;
        }

        public void setTimeOfUnloading(Time timeOfUnloading) {
            this.timeOfUnloading = timeOfUnloading;
        }
    }
}
