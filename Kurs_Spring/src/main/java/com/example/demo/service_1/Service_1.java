package com.example.demo.service_1;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class Service_1 {

    @GetMapping("/startSchedule")
    public static ArrayList<Options.Ship> getSchedule(@RequestParam("count") int count) {
        ArrayList<Options.Ship> result = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            Options.Ship temp = new Options.Ship();
            result.add(temp);
        }
        return result;
    }
}
