package com.example.demo.service_3;

import com.example.demo.service_1.Options;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class Crane implements Callable<List<ScheduleNote>> {
    private final List<Options.Ship> queue;
    private final boolean isGetStatistic;
    private final boolean isSecond;

    Crane(List<Options.Ship> queue, boolean isGetStatistic, boolean isSecond) {
        this.queue = queue;
        this.isGetStatistic = isGetStatistic;
        this.isSecond = isSecond;
    }

    @Override
    public List<ScheduleNote> call() {
        List<ScheduleNote> scheduleNotes = new ArrayList<>();
        if (!isGetStatistic) {
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                System.err.println(e.getMessage());
                e.printStackTrace();
            }
        } else if (!isSecond) {
            for (int i = 0; i < queue.size(); i++) {
                    Options.Time temp = new Options.Time(Service_3.waitings.get(queue.get(i)).getDays(),
                            Service_3.waitings.get(queue.get(i)).getHours(), Service_3.waitings.get(queue.get(i)).getMinutes());
                    temp.addTime(queue.get(i).getTime());
                    ScheduleNote ship = new ScheduleNote(queue.get(i).getName(), queue.get(i).getTime(), Service_3.waitings.get(queue.get(i)),
                            temp, queue.get(i).getTimeOfUnloading());
                    scheduleNotes.add(ship);
                boolean isCount = false;
                int tempLength = 0;
                for (int j = i + 1; j < queue.size(); j++) {
                    if (queue.get(i).isLinger(queue.get(j), Service_3.waitings.get(queue.get(i)).fullMinutes(), 0)) {
                        if (!isCount) {
                            isCount = true;
                            synchronized (Service_3.countQueue) {
                                Service_3.countQueue++;
                            }
                        }
                        tempLength++;
                    }
                }
                synchronized (Service_3.sumQueue) {
                    Service_3.sumQueue += tempLength;
                }
            }
        }
        return scheduleNotes;
    }
}
