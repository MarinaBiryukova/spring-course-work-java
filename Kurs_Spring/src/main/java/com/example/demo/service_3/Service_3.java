package com.example.demo.service_3;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.example.demo.service_1.Options;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.*;

public class Service_3 {

    private static final Integer PENALTY = 100;
    private static final Integer CRANE_COST = 30000;
    private static Integer looseCount = 1;
    private static Integer liquidCount = 1;
    private static Integer containerCount = 1;
    private static final Map<String,List<List<Options.Ship>>> craneQueues = new TreeMap<>();
    static Map<Options.Ship, Options.Time> waitings = new TreeMap<>();
    public static Integer sumQueue = 0;
    public static Integer countQueue = 0;

    public static Comparator<Options.Ship> timeComparator = Comparator.comparing(Options.Ship::getTime);

    public static Schedule getStatistic(int count) {
        String url = "http://localhost:8080/jsonStartSchedule?count=" + count;
        URL URL = null;
        try {
            URL = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        BufferedReader reader = null;
        try {
            HttpURLConnection connection = null;
            if (URL != null) {
                connection = (HttpURLConnection) URL.openConnection();
            }
            if (connection != null) {
                connection.setRequestMethod("GET");
                reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        ArrayList<Options.Ship> schedule = new ArrayList<>();
        String path = null;
        try {
            if (reader != null) {
                path = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileReader jsonFile = null;
        try {
            if (path != null) {
                jsonFile = new FileReader(path);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (jsonFile != null) {
            reader = new BufferedReader(jsonFile);
        }
        ObjectMapper mapper = new ObjectMapper();
        String obj = null;
        try {
            if (reader != null) {
                obj = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        int i = 0;
        while (obj != null) {
            Options.Ship ship;
            try {
                mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                ship = mapper.readValue(obj, Options.Ship.class);
                schedule.add(ship);
                i++;
                if (i > count) {
                    count++;
                }
                obj = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ArrayList<Delays> delays = Service_3.getDelays(count);
        for (i = 0; i < count; i++) {
            schedule.get(i).addDaysToArrive(delays.get(i).getDays());
            schedule.get(i).addMinutesToUnloading(delays.get(i).getMinutes());
        }
        schedule.sort(timeComparator);
        Integer prev = getPenalty(schedule, delays, looseCount, liquidCount, containerCount);
        while (true) {
            Integer firstCrane = getPenalty(schedule, delays,
                    looseCount + 1, liquidCount, containerCount);
            Integer secondCrane = getPenalty(schedule, delays, looseCount,
                    liquidCount + 1, containerCount);
            Integer thirdCrane = getPenalty(schedule, delays, looseCount, liquidCount,
                    containerCount + 1);
            if (firstCrane <= secondCrane && firstCrane <= thirdCrane) {
                if (prev - firstCrane >= CRANE_COST) {
                    prev = firstCrane;
                    looseCount++;
                }
                else {
                    break;
                }
            }
            else if (secondCrane <= firstCrane && secondCrane <= thirdCrane) {
                if (prev - secondCrane >= CRANE_COST) {
                    prev = secondCrane;
                    liquidCount++;
                }
                else {
                    break;
                }
            }
            else {
                if (prev - thirdCrane >= CRANE_COST) {
                    prev = thirdCrane;
                    containerCount++;
                }
                else {
                    break;
                }
            }
        }
        return generateStatistic(schedule, delays, count);
    }

    public static ArrayList<Delays> getDelays(Integer count)  {
        ArrayList<Delays> result = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            Delays temp = new Delays();
            result.add(temp);
        }
        return result;
    }

    private static Integer getPenalty(List<Options.Ship> schedule, List<Delays> delays,
                                      Integer looseCount, Integer liquidCount, Integer containerCount) {
        ArrayList<Options.Ship> ships = new ArrayList<>();
        for (Options.Ship s : schedule) {
            ships.add(new Options.Ship(s));
        }
        Map<String, Integer> cranes = new TreeMap<>();
        cranes.put("loose", looseCount);
        cranes.put("liquid", liquidCount);
        cranes.put("container", containerCount);
        craneQueues.clear();
        waitings.clear();
        for (String type : cranes.keySet()) {
            craneQueues.put(type, new ArrayList<>());
            for (int i = 0; i < cranes.get(type); i++) {
                craneQueues.get(type).add(new ArrayList<>());
            }
        }
        for (Options.Ship ship : ships) {
            waitings.put(ship, new Options.Time(0,0,0));
        }
        for (Options.Ship ship : ships) {
            int index = getCraneIndex(craneQueues.get(ship.getType()), ship);
            if (index != craneQueues.get(ship.getType()).size() - 1) {
                craneQueues.get(ship.getType()).get(index + 1).add(ship);
                ship.recalculateTimeOfUnloading(2);
            }
            if (craneQueues.get(ship.getType()).get(index).size() > 0) {
                int size = craneQueues.get(ship.getType()).get(index).size();
                for (int j = 0; j < size; j++) {
                    Options.Ship prev = craneQueues.get(ship.getType()).get(index).get(j);
                    if (prev.isLinger(ship, waitings.get(prev).fullMinutes(), waitings.get(ship).fullMinutes())) {
                        waitings.get(ship).addTime(prev.getTimeOfDelay(ship, waitings.get(prev).fullMinutes(),
                                waitings.get(ship).fullMinutes()));
                    }
                }
            }
            craneQueues.get(ship.getType()).get(index).add(ship);
        }
        Options.Time penaltyTime = new Options.Time(0, 0, 0);
        for (Delays delay : delays) {
            penaltyTime.addMinutes(delay.getMinutes());
        }
        for (Options.Time wait : waitings.values()) {
            penaltyTime.addTime(wait);
        }
        ExecutorService service = Executors.newFixedThreadPool(liquidCount + looseCount + containerCount);
        for (String type : cranes.keySet()) {
            int cranesCount = cranes.get(type);
            for (int i = 0; i < cranesCount; i++) {
                if (cranesCount > 1) {
                    if (i % 2 == 0) {
                        service.submit(new Crane(craneQueues.get(type).get(i), false, false));
                    }
                    else {
                        service.submit(new Crane(craneQueues.get(type).get(i), false, true));
                    }
                }
                else {
                    service.submit(new Crane(craneQueues.get(type).get(i), false, false));
                }
            }
        }
        service.shutdown();
        try {
            service.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return (penaltyTime.getDays() * 24 + penaltyTime.getHours()) * PENALTY;
    }

    private static Schedule generateStatistic(List<Options.Ship> schedule,
                                              List<Delays> delays, Integer count) {
        ArrayList<Options.Ship> ships = new ArrayList<>();
        for (Options.Ship s : schedule) {
            ships.add(new Options.Ship(s));
        }
        Map<String, Integer> cranes = new TreeMap<>();
        cranes.put("loose", looseCount);
        cranes.put("liquid", liquidCount);
        cranes.put("container", containerCount);
        craneQueues.clear();
        waitings.clear();
        for (String type : cranes.keySet()) {
            craneQueues.put(type, new ArrayList<>());
            for (int i = 0; i < cranes.get(type); i++) {
                craneQueues.get(type).add(new ArrayList<>());
            }
        }
        for (Options.Ship ship : ships) {
            waitings.put(ship, new Options.Time(0,0,0));
        }
        for (Options.Ship ship : ships) {
            int index = getCraneIndex(craneQueues.get(ship.getType()), ship);
            if (index != craneQueues.get(ship.getType()).size() - 1) {
                craneQueues.get(ship.getType()).get(index + 1).add(ship);
                ship.recalculateTimeOfUnloading(2);
            }
            if (craneQueues.get(ship.getType()).get(index).size() > 0) {
                int size = craneQueues.get(ship.getType()).get(index).size();
                for (int j = 0; j < size; j++) {
                    Options.Ship prev = craneQueues.get(ship.getType()).get(index).get(j);
                    if (prev.isLinger(ship, waitings.get(prev).fullMinutes(), waitings.get(ship).fullMinutes())) {
                        waitings.get(ship).addTime(prev.getTimeOfDelay(ship, waitings.get(prev).fullMinutes(),
                                waitings.get(ship).fullMinutes()));
                    }
                }
            }
            craneQueues.get(ship.getType()).get(index).add(ship);
        }
        Options.Time penaltyTime = new Options.Time(0, 0, 0);
        Options.Time sumWaitings = new Options.Time(0,0,0);
        for (Delays delay : delays) {
            penaltyTime.addMinutes(delay.getMinutes());
        }
        for (Options.Time wait : waitings.values()) {
            penaltyTime.addTime(wait);
        }
        waitings.values().forEach(sumWaitings::addTime);
        ExecutorService service = Executors.newCachedThreadPool();
        ExecutorCompletionService<List<ScheduleNote>> completionService = new ExecutorCompletionService<>(service);
        for (String type : cranes.keySet()) {
            int cranesCount = cranes.get(type);
            for (int i = 0; i < cranesCount; i++) {
                if (cranesCount > 1) {
                    if (i % 2 == 0) {
                        completionService.submit(new Crane(craneQueues.get(type).get(i), true, false));
                    }
                    else {
                        completionService.submit(new Crane(craneQueues.get(type).get(i), true, true));
                    }
                }
                else {
                    completionService.submit(new Crane(craneQueues.get(type).get(i), true, false));
                }
            }
        }
        List<ScheduleNote> notes = new ArrayList<>();
        for (int i = 0; i < looseCount + liquidCount + containerCount; i++) {
            try {
                Future<List<ScheduleNote>> temp = completionService.take();
                notes.addAll(temp.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        final int[] sumOfDelays = {0};
        delays.forEach((delay) -> sumOfDelays[0] += delay.getMinutes());
        return new Schedule(notes, count, (double)sumQueue / countQueue,
                sumWaitings.divide(waitings.values().size()), Collections.max(delays).getMinutes(),
                (int)(sumOfDelays[0] / (long) delays.size()), (penaltyTime.getDays() * 24 + penaltyTime.getHours()) * PENALTY,
                looseCount, liquidCount, containerCount);
    }

    private static Options.Time getEndTime(Options.Ship ship, Integer waiting) {
        Options.Time result = new Options.Time(0,0,0);
        result.addTime(ship.getTimeOfUnloading());
        result.addMinutes(waiting);
        result.addTime(ship.getTime());
        return result;
    }

    private static int getCraneIndex(List<List<Options.Ship>> cranes, Options.Ship ship) {
        for (int i = 0; i < cranes.size(); i++) {
            if (cranes.get(i).size() == 0 || !(cranes.get(i).get(cranes.get(i).size() - 1).isLinger(ship,
                    waitings.get(cranes.get(i).get(cranes.get(i).size() - 1)).fullMinutes(),
                    waitings.get(ship).fullMinutes()))) {
                return i;
            }
        }
        Options.Time min = new Options.Time(Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE);
        int index = 0;
        for (int i = 0; i < cranes.size(); i++) {
            Options.Time temp = getEndTime(cranes.get(i).get(cranes.get(i).size() - 1),
                    waitings.get(cranes.get(i).get(cranes.get(i).size() - 1)).fullMinutes());
            if (temp.equals(min)) {
                min = temp;
                index = i;
            }
        }
        return index;
    }
}
