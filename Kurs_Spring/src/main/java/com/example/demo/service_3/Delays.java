package com.example.demo.service_3;

import java.io.PrintStream;

public class Delays implements Comparable<Delays>{
    private final Integer days;
    private final Integer minutes;

    public Delays() {
        this.days = (int)(Math.random() * 15) - 7;
        this.minutes = (int)(Math.random() * 1441) / 10 * 10;
    }

    public Integer getDays() {
        return days;
    }

    public Integer getMinutes() {
        return minutes;
    }

    public void print(PrintStream output) {
        output.print("Arrive delay: " + this.days + ". Unloading delay: " + this.minutes + "\n");
    }

    @Override
    public int compareTo(Delays delays) {
        return this.minutes - delays.minutes;
    }
}
