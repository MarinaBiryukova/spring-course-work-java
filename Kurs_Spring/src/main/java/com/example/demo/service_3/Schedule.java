package com.example.demo.service_3;

import com.example.demo.service_1.Options;

import java.util.List;

public class Schedule {
    private List<ScheduleNote> scheduleNotes;
    private int countOfShips;
    private double avgQueueLen;
    private Options.Time avgWaitingTime;
    private int maxDelayTime;
    private double avgDelayTime;
    private int penaltyCost;
    private int looseCount;
    private int liquidCount;
    private int containerCount;

    public Schedule(List<ScheduleNote> scheduleNotes, int countOfShips, double avgQueueLen, Options.Time avgWaitingTime,
                    int maxDelayTime, double avgDelayTime, int penaltyCost, int looseCount, int liquidCount,
                    int containerCount) {
        this.scheduleNotes = scheduleNotes;
        this.countOfShips = countOfShips;
        this.avgQueueLen = avgQueueLen;
        this.avgWaitingTime = avgWaitingTime;
        this.maxDelayTime = maxDelayTime;
        this.avgDelayTime = avgDelayTime;
        this.penaltyCost = penaltyCost;
        this.looseCount = looseCount;
        this.liquidCount = liquidCount;
        this.containerCount = containerCount;
    }

    public List<ScheduleNote> getScheduleNotes() {
        return scheduleNotes;
    }

    public int getCountOfShips() {
        return countOfShips;
    }

    public double getAvgQueueLen() {
        return avgQueueLen;
    }

    public Options.Time getAvgWaitingTime() {
        return avgWaitingTime;
    }

    public int  getMaxDelayTime() {
        return maxDelayTime;
    }

    public double getAvgDelayTime() {
        return avgDelayTime;
    }

    public int getPenaltyCost() {
        return penaltyCost;
    }

    public int getLooseCount() {
        return looseCount;
    }

    public int getLiquidCount() {
        return liquidCount;
    }

    public int getContainerCount() {
        return containerCount;
    }

    public void setScheduleNotes(List<ScheduleNote> scheduleNotes) {
        this.scheduleNotes = scheduleNotes;
    }

    public void setCountOfShips(int countOfShips) {
        this.countOfShips = countOfShips;
    }

    public void setAvgQueueLen(double avgQueueLen) {
        this.avgQueueLen = avgQueueLen;
    }

    public void setAvgWaitingTime(Options.Time avgWaitingTime) {
        this.avgWaitingTime = avgWaitingTime;
    }

    public void setMaxDelayTime(int maxDelayTime) {
        this.maxDelayTime = maxDelayTime;
    }

    public void setAvgDelayTime(double avgDelayTime) {
        this.avgDelayTime = avgDelayTime;
    }

    public void setPenaltyCost(int penaltyCost) {
        this.penaltyCost = penaltyCost;
    }

    public void setLooseCount(int looseCount) {
        this.looseCount = looseCount;
    }

    public void setLiquidCount(int liquidCount) {
        this.liquidCount = liquidCount;
    }

    public void setContainerCount(int containerCount) {
        this.containerCount = containerCount;
    }
}
