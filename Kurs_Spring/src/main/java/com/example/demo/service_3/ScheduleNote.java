package com.example.demo.service_3;

import com.example.demo.service_1.Options;

public class ScheduleNote {
    private String shipName;
    private Options.Time arriveTime;
    private Options.Time waitingTime;
    private Options.Time startingTime;
    private Options.Time unloadingTime;

    public ScheduleNote(String shipName, Options.Time arriveTime, Options.Time waitingTime, Options.Time startingTime,
                        Options.Time unloadingTime) {
        this.shipName = shipName;
        this.arriveTime = arriveTime;
        this.waitingTime = waitingTime;
        this.startingTime = startingTime;
        this.unloadingTime = unloadingTime;
    }

    public ScheduleNote() {
    }

    public String getShipName() {
        return shipName;
    }

    public Options.Time getArriveTime() {
        return arriveTime;
    }

    public Options.Time getWaitingTime() {
        return waitingTime;
    }

    public Options.Time getStartingTime() {
        return startingTime;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public void setArriveTime(Options.Time arriveTime) {
        this.arriveTime = arriveTime;
    }

    public void setWaitingTime(Options.Time waitingTime) {
        this.waitingTime = waitingTime;
    }

    public void setStartingTime(Options.Time startingTime) {
        this.startingTime = startingTime;
    }

    public Options.Time getUnloadingTime() {
        return unloadingTime;
    }

    public void setUnloadingTime(Options.Time unloadingTime) {
        this.unloadingTime = unloadingTime;
    }
}
